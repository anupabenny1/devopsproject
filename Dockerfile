FROM library/tomcat
ADD target/*.jar /usr/local/tomcat/webapps
WORKDIR /usr/local/tomcat/webapps
CMD ["java", "-jar", "bootcamp-0.0.1-SNAPSHOT.jar"]
